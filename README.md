# Landmark remark

### Fontend
Landmark remarks uses 

* [AngularJS]
* [Jquery] 

### Backend

* [Asp MVC]
* [Asp Web Api]
* [Entity framework]
* [Identity v2]

### Installation
Landmark remarks requires [Asp.net] v4.5+ and MVC 5 to run. The project is using code first migration.

    1. Clone the project using visual studio.
    2. Restore the nuget by right clicking on the solution project "Restore nuget packages"
    3. Open packages manager console 
    4. enter "update-database" (this will create a localdb using mdf)
    5. domain require to be localhost due to google map api HTTP referrer
    
### Time spend
    - 2 hour on HTML/CSS
    - 1 hour on backend
    - 5 hour on SPA
	
### future development
	- Loading screen
	- infinite scroll for remarks
	
### Issue
    - Mobile responisve testing is only done on a few phone screen size, others screen size might have some misalignment issue in layout
    - IE doesn't support navigator for GPS
	- ES6 is used in the project and it is not supported on Internet explorer. Recommend google chrome, edge browser.

