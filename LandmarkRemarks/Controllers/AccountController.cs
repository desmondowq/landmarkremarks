﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using LandmarkRemarks.Models;
using LandmarkRemarks.Providers;
using LandmarkRemarks.Results;

namespace LandmarkRemarks.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {

        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [Route("IsAuthorize")]
        [HttpGet]
        public IHttpActionResult IsAuthorize()
        {
            return Ok();
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(getModelstateErrorMessages(ModelState));
            }

            var user = new ApplicationUser() { UserName = model.Username };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                string errorMessages = string.Empty;
                if (result.Errors != null)
                {
                    errorMessages = string.Join("|", result.Errors);
                }

                return BadRequest(errorMessages);
            }

            return null;
        }


        public static string getModelstateErrorMessages(ModelStateDictionary modelState)
        {
            string errorMessage = "Invalid Validation";
            if (modelState.Values.Count > 0)
            {
                foreach (ModelState state in modelState.Values)
                {
                    foreach (ModelError error in state.Errors)
                    {
                        errorMessage += string.IsNullOrEmpty(errorMessage) ? error.ErrorMessage : "|" + error.ErrorMessage;
                    }
                }
                return errorMessage;
            }
            else
            {
                return string.Empty;
            }
        }





        #endregion
    }
}
