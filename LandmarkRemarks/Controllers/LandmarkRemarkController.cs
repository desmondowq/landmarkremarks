﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LandmarkRemarks.Models;
using LandmarkRemarks.Utils;
using Microsoft.AspNet.Identity;

namespace LandmarkRemarks.Controllers
{
    [Authorize]
    [RoutePrefix("api/LandmarkRemark")]
    public class LandmarkRemarkController : ApiController
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        [Route("user")]
        public async Task<IHttpActionResult> user()
        {
            var userId = User.Identity.GetUserId();

            var locations = await db.LandmarkRemarks
                                    .Include(i => i.User)
                                    .Where(a => a.UserId == userId)
                                    .OrderByDescending(n => n.DateCreated)
                                    .ToListAsync();

            return Ok(locations
                    .OrderByDescending(o => o.DateCreated)
                    .Select(s => new LandmarkRemarkViewModel
                    {
                        Id = s.Id,
                        Remark = s.Remarks,
                        Username = s.User.UserName,
                        Latitude = s.Geo.Latitude.Value,
                        Longitude = s.Geo.Longitude.Value,
                        Self = true
                    })
                    .ToList());
        }

        // GET: api/LocationRemarks
        [AllowAnonymous]
        public async Task<IHttpActionResult> Get(string search)
        {

            IQueryable<LandmarkRemark> remarks = db.LandmarkRemarks
                .Include(i => i.User);

            if (!string.IsNullOrWhiteSpace(search))
            {
                remarks = remarks.Where(a => a.User.UserName.Contains(search) || a.Remarks.Contains(search));
            }

            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                remarks = remarks.Where(a => !a.UserId.Equals(userId));
            }

            var locations = await remarks
                .OrderByDescending(n => n.DateCreated)
                .ToListAsync();

            return Ok(locations.Select(s => new LandmarkRemarkViewModel
            {
                Id = s.Id,
                Username = s.User.UserName,
                Remark = s.Remarks,
                Latitude = s.Geo.Latitude.Value,
                Longitude = s.Geo.Longitude.Value,
                Self = false
            }).ToList());
        }

        // POST: api/LocationRemarks
        [HttpPost]
        public async Task<IHttpActionResult> Post(LandmarkBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var remark = new LandmarkRemark
                {
                    UserId = User.Identity.GetUserId(),
                    Geo = GeoUtils.CreatePoint(model.Latitude, model.Longitude),
                    Remarks = model.Remark,
                    DateCreated = DateTime.UtcNow.AddHours(8),
                    DateUpdated = DateTime.UtcNow.AddHours(8)
                };

                db.LandmarkRemarks.Add(remark);
                await db.SaveChangesAsync();

                return Ok(new
                {
                    Message = "Landmark remark successfully added",
                    AddRemarkId = remark.Id
                });
            }

            return BadRequest("There is an error in saving the given remarks, please make sure that your GPS is turn on");
        }

        // DELETE: api/LocationRemarks/5
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            var userId = User.Identity.GetUserId();
            if (db.LandmarkRemarks.Any(a => a.UserId == userId && a.Id == id))
            {
                var remark = new LandmarkRemark
                {
                    Id = id
                };

                db.LandmarkRemarks.Attach(remark);
                db.LandmarkRemarks.Remove(remark);
                await db.SaveChangesAsync();
                return Ok();
            }

            return BadRequest("There is an error performing the request, please try again later");
        }
    }
}
