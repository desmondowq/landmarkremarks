﻿angular.module('LandmarkRemarkApp.controllers')
    .controller('AppController',
        function (MapService, $scope, AuthService, $rootScope, $state) {
            console.log("load app ctrl");
            MapService.drawMap("map-canvas");

            //watch is authorize in case user was found sign in
            $scope.$watch('isAuthorize', function () {
                $scope.btnTxt = "Open Menu";
            });

            $scope.toggleSidebar = function () {
                $('#sidebar').toggleClass('active');
                //if (!$rootScope.isAuthorize && $state.current.name !== 'app.register-login') {
                //    console.log($rootScope.isAuthorize, 'not login');
                //    $state.go('app.register-login');
                //}
            };

            $rootScope.isAuthorize = false;

            //check is user is already sign in.
            AuthService.isAuthorize().then(function (res) {
                $rootScope.isAuthorize = true;
            }, function (error) {
                $rootScope.isAuthorize = false;
            });

            $scope.onLogout = function () {
                AuthService.logout();
                $rootScope.isAuthorize = false;
                console.log($state.current.name);
                if ($state.current.name !== 'app.home') {
                    $state.go('app.home');
                }
            };

        });