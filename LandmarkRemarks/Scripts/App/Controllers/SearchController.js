﻿angular.module('LandmarkRemarkApp.controllers')
    .controller('SearchController',
        function ($scope, remarks, LandmarkService, MapService, $state) {
            //map service set user marks
            $scope.remarks = remarks;
            console.log("load search controller");
            $scope.searchRemark = "";

            MapService.clearRemarkMarkers();
            MapService.setSearchRemarksMarker(remarks);

            $scope.showSearch = $state.current.name === 'app.search';

            $scope.onRemarkSelect = function (id) {
                console.log(angular.element(angular.element(document.querySelector('#sidebar'))).hasClass('active'));
                if (angular.element(angular.element(document.querySelector('#sidebar'))).hasClass('active')) {
                    $('#sidebar').toggleClass('active');
                }

                MapService.selectRemarkMarker(id);
            };

            $scope.onRemarkDelete = function(index, id) {
                $scope.remarks.splice(index, 1);
                MapService.removeRemarkMarker(id);
                LandmarkService.delete(id).then(function(res) {
                        swal("Successfully delete remark");
                    },
                    function(error) {
                        swal("Oops...", "There is an error in deleting the remark try again later");

                    });
            };

            $scope.onSearchRemark = function() {
                console.log("$scope.searchRemark", $scope.searchRemark);
                if ($scope.searchRemark.length) {
                    LandmarkService.query($scope.searchRemark).then(function(res) {
                            console.log(JSON.stringify(res), 'remarks in StateConfig resolve');
                            $scope.remarks = res;
                            MapService.clearRemarkMarkers();
                            MapService.setSearchRemarksMarker(res);
                        },
                        function(error) {
                            $scope.remarks = [];
                        });
                } else {
                    $scope.remarks = [];
                    MapService.clearRemarkMarkers();
                }


            };
        });