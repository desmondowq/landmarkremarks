﻿angular.module('LandmarkRemarkApp.controllers')

    .controller('LoginRegisterController',
        function ($scope, $state, AuthService) {
            console.log("load login and register form");
            $('#login-form-link').click(function (e) {
                $("#login-form").delay(100).fadeIn(100);
                $("#register-form").fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
            $('#register-form-link').click(function (e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });

            $scope.onLoginSubmit = function() {
                console.log("login submitted!");
                AuthService.login($scope.loginData.username, $scope.loginData.password).then(function(res) {
                        console.log(JSON.stringify(res), 'success response in LoginController');
                        $scope.isAuthorize = true;
                        $state.go('app.home');
                    },
                    function(error) {
                        console.log('error in LoginController 1: ', JSON.stringify(error));
                        swal("Login error!",
                            error.data && error.data.error_description ? error.data.error_description : 'Login Failed',
                            "error");
                    }
                );
            };

            $scope.onRegisterSubmit = function() {
                console.log("register submitted!");
                AuthService.register($scope.registerData).then(function(result) {
                        AuthService.login($scope.registerData.username, $scope.registerData.password).then(
                            function(res) {
                                console.log(JSON.stringify(res), 'success response in LoginController');
                                $scope.isAuthorize = true;
                                $state.go('app.home');
                            },
                            function(error) {
                                console.log('error in LoginController 1: ', JSON.stringify(error));
                                swal("Login error!",
                                    error.data && error.data.error_description
                                    ? error.data.error_description
                                    : 'Login Failed',
                                    "error");
                            }
                        );
                    },
                    function(error) {
                        var errorMessage = '';
                        if (error.data && error.data.message) {

                            angular.forEach(error.data.message.split('|'),
                                function(errorMessageLine) {
                                    if (errorMessageLine.length) {
                                        errorMessageLine += '\n';
                                    }
                                    errorMessage += errorMessageLine;
                                });
                        }


                        swal("Error in registration!", errorMessage, "error");

                    });
            };
        });


