﻿angular.module('LandmarkRemarkApp.controllers')

    .controller('HomeController',
        function ($scope, MapService, AuthService, $state, LandmarkService, $rootScope) {
            console.log("load home controller");
            $scope.remark = "";
            $scope.searchRemark = "";
            $scope.RemarkBtnValue = "Login / Sign up now";
            
            $rootScope.isAuthorize = false;

            //check is user is already sign in.
            AuthService.isAuthorize().then(function (res) {
                $rootScope.isAuthorize = true;
            }, function (error) {
                $rootScope.isAuthorize = false;
            });

            $scope.$watch('isAuthorize', function () {
                if ($rootScope.isAuthorize) {
                    $scope.RemarkBtnValue = "Add A Remark";
                } else {
                    $scope.RemarkBtnValue = "Login / Sign up now";
                }
            });


            $scope.OnRemarkBtnClick = function() {


                AuthService.isAuthorize().then(function(res) {
                        $rootScope.isAuthorize = true;
                        if ($scope.remark.length < 256 && $scope.remark.length > 3) {
                            //add user login.
                            console.log($scope.remark, "remark");

                            //for testing allow user map to be drag
                            var pos = MapService.getUserMarkerPos();

                            LandmarkService.post(pos.lat(), pos.lng(), $scope.remark).then(function(res) {
                                    console.log(JSON.stringify(res), 'res from post remarks');
                                    $state.go('app.user-remarks');
                                },
                                function(err) {
                                    console.log(JSON.stringify(err), 'err from post remarks');
                                });
                        } else {
                            swal("Ooops", "The min length of remark is 3 and max length is 256");
                        }


                    },
                    function(error) {
                        $rootScope.isAuthorize = false;
                        console.log("redirect to login");
                        $state.go('app.register-login');
                    });
            };

            $scope.OnSearchRemark = function() {

                $state.go('app.search',
                    {
                        'search': $scope.searchRemark
                    });
            };


        });


