﻿angular.module('LandmarkRemarkApp.services')
    .service('AuthService',
        function ($http, EndPoints, $q, $localStorage, STORAGE_KEY, $rootScope) {

            var service = this;

            service.isAuthorize = function () {
                console.log("check authorize");
                return EndPoints.accountIsAuthorize().$promise.then(function (res) {
                    console.log(JSON.stringify(res), 'backend.getAuthentication success');
                    return res;
                }, function(error) {
                    console.log(JSON.stringify(error), 'backend.getAuthentication error');

                    //remove accesstoken if user is not authorize
                    $localStorage.remove(STORAGE_KEY.accessToken);
                    $localStorage.remove(STORAGE_KEY.username);
                    $http.defaults.headers.common.Authorization = undefined;

                    return $q.reject(error);
                });
            }

            service.register = function (registerData) {
                console.log(JSON.stringify(registerData), 'registerData');
                return EndPoints.accountRegister(registerData).$promise.then(function (res) {
                    console.log(JSON.stringify(res), 'res after register');
                    return res;
                }, function (error) {
                    console.log(JSON.stringify(error), 'backend.register error');
                    return $q.reject(error);
                });
            }

            service.login = function(username, password) {
                return EndPoints.accountLogin(username, password).$promise.then(function (serverAuthResponse) {
                    console.log(JSON.stringify(serverAuthResponse), 'serverAuthResponse from backend.login');
                    service.setAccessToken(username, serverAuthResponse.access_token);
                    $rootScope.username = username ? username : '';
                }, function (error) {
                    console.log(JSON.stringify(error), 'backend.login error');
                    return $q.reject(error);
                });
            }

            service.setAccessToken = function (username, accessToken) {
                $localStorage.set(STORAGE_KEY.accessToken, accessToken);
                $localStorage.set(STORAGE_KEY.username, username);
                $http.defaults.headers.common.Authorization = 'bearer ' + accessToken;
            }

            service.logout = function() {
                $localStorage.remove(STORAGE_KEY.accessToken);
                $localStorage.remove(STORAGE_KEY.username);
                $http.defaults.headers.common.Authorization = undefined;
            }

        });