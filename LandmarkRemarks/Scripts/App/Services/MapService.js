﻿angular.module('LandmarkRemarkApp.services')
    .service('MapService',
        function ($interval) {
            var service = this;

            var map;
            var DEFAULT_CENTER_LAT = 1.3520830;
            var DEFAULT_CENTER_LNG = 103.8198360;
            var DEFAULT_CENTER = new google.maps.LatLng(DEFAULT_CENTER_LAT, DEFAULT_CENTER_LNG);
            var userLocationMarker;
            var otherUserIconTemplate = "/content/images/other-user-remark.png";
            var UserIconTemplate = "/content/images/user-remark.png";

            service.latitude = null;
            service.longitude = null;

            var searchRemarkMarkers = [];
            var searchRemarkInfoWindows = [];

            var geoSuccess = function geoSuccess(position) {
                //prevent multiple users marker stack at the same location
                if (userLocationMarker) {
                    var markerPos = userLocationMarker.getPosition();
                    if (position.coords.latitude !== markerPos.lat() || position.coords.longitude !== markerPos.lng()) {
                        userLocationMarker.setMap(null);
                        setUserMarker(position);
                    }
                } else {
                    setUserMarker(position);
                }
            };

            function setUserMarker(position) {
                console.log("set user location");

                service.latitude = position.coords.latitude;
                service.longitude = position.coords.longitude;

                userLocationMarker = new google.maps.Marker({
                    map: map,
                    position: { lat: position.coords.latitude, lng: position.coords.longitude },
                    draggable: true
                });

                map.setZoom(17);
                map.panTo({ lat: position.coords.latitude, lng: position.coords.longitude });
            }


            var geoError = function (error) {
                switch (error.code) {
                case error.PERMISSION_DENIED:
                    swal("error", "You have denied the request for Geolocation. You can reset the setting at your browser setting page.", "error");
                    break;
                case error.POSITION_UNAVAILABLE:
                    swal("error", "Unable to retrieve your location.", "error");
                    break;
                }
            };

            var setUserAccessLocation = function() {
                if (navigator.geolocation) {
                    navigator.permissions.query({ name: 'geolocation' }).then(function(PermissionStatus) {
                        if ('granted' === PermissionStatus.state) {
                            navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
                        } else if ('denied' === PermissionStatus.state) {
                            swal("error",
                                "You have denied the request for Geolocation. You can reset the setting at your browser setting page.",
                                "error");
                        } else {
                            swal('Access user Location',
                                    "You are require to select allow on the pop up to allow the system to retrieve your current location.")
                                .then((value) => {
                                    defer.resolve();
                                });
                        }
                    });
                } else {
                    swal("error", "The browser you use doesn't support geolocation.", "error");
                }
            };

            service.drawMap = function (divId) {
                if (!map) {
                    var mapOptions = {
                        zoom: 12,
                        center: DEFAULT_CENTER,
                        gestureHandling: 'greedy'
                    };

                    map = new google.maps.Map(document.getElementById(divId), mapOptions);
                }

                google.maps.event.addListener(map, "idle", function () {
                    if (!userLocationMarker) {
                        setUserAccessLocation();
                        intervalLoadUserLocation();
                    }

                    google.maps.event.trigger(map, 'resize');
                });

            };


            function intervalLoadUserLocation() {
                $interval(setUserAccessLocation, 5000);
            }

            service.getUserMarkerPos = function() {
                if (userLocationMarker) {
                    return userLocationMarker.getPosition();
                }
                return;
            };

            service.setSearchRemarksMarker = function(remarks) {
                angular.forEach(remarks,
                    function(remark) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(remark.latitude, remark.longitude),
                            map: map,
                            icon: remark.self ? UserIconTemplate : otherUserIconTemplate,
                            id: remark.id,
                            username: remark.username,
                            content: remark.remark
                        });

                        addInfoWindow(marker, remark.username + " - " + remark.remark, remark.id);

                        searchRemarkMarkers.push(marker);
                    });
            };

            function addInfoWindow(marker, message, id) {

                var infoWindow = new google.maps.InfoWindow({
                    content: message.charAt(0).toUpperCase() + message.slice(1),
                    id: id
                });

                searchRemarkInfoWindows.push(infoWindow);

                google.maps.event.addListener(marker, 'click', function () {
                    closeAllInfoWindows();
                    infoWindow.open(map, marker);
                });
            }

            function closeAllInfoWindows() {
                for (var i = 0; i < searchRemarkInfoWindows.length; i++) {
                    searchRemarkInfoWindows[i].close();
                }
            }

            service.selectRemarkMarker = function(id) {
                //var remark = $scope.remarks.find(item => item.id === id);
                closeAllInfoWindows();
                var infoWindow = searchRemarkInfoWindows.find(item => item.id === id);
                var marker = searchRemarkMarkers.find(item => item.id === id);
                infoWindow.open(map, marker);
                map.panTo(marker.getPosition());
                map.setZoom(17);
            };

            service.removeRemarkMarker = function(id) {
                var marker = searchRemarkMarkers.find(item => item.id === id);
                var index = searchRemarkMarkers.indexOf(marker);
                searchRemarkMarkers.splice(index, 1);
                searchRemarkInfoWindows.splice(index, 1);
                marker.setMap(null);
            };

            service.clearRemarkMarkers = function() {
                for (var i = 0; i < searchRemarkMarkers.length; i++) {
                    searchRemarkMarkers[i].setMap(null);
                }
                searchRemarkMarkers = [];
                searchRemarkInfoWindows = [];
            };

        });