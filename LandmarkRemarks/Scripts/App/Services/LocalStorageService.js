﻿angular.module('LandmarkRemarkApp.services')

    .service('$localStorage', function ($window) {
        var localStorage = this;

        localStorage.set = function (key, value) {
            if (angular.isObject(value)) {
                console.warn('should call $localStorage.setObject instead of $localStorage.set');
            }
            $window.localStorage[key] = value;
        };

        localStorage.get = function (key, defaultValue) {
            if ($window.localStorage[key] && angular.isObject($window.localStorage[key])) {
                console.warn('should call $localStorage.getObject instead of $localStorage.get');
            }
            return $window.localStorage[key] || defaultValue;
        };

        localStorage.setObject = function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        };

        localStorage.getObject = function (key, defaultValue) {
            if ($window.localStorage[key]) {
                return JSON.parse($window.localStorage[key]);
            }
            else {
                return defaultValue;
            }
        };

        localStorage.remove = function(key) {
            $window.localStorage.removeItem(key);
        };
    });