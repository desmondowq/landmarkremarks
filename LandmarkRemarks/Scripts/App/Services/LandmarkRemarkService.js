﻿angular.module('LandmarkRemarkApp.services')
    .service('LandmarkService',
        function (EndPoints, $q, $localStorage, STORAGE_KEY) {

            var service = this;

            service.query = function (data) {
                return EndPoints.getLocationRemarks(data).$promise.then(function (res) {
                    console.log("landmark remark list successfully retrieved", JSON.stringify(res));
                    return res;
                },
                    function (error) {
                        console.error("Error in retrieving the list", JSON.stringify(error));
                    });
            }

            service.post = function (lat, lng, remark) {
                return EndPoints.postLandmarkRemark(lat, lng, remark).$promise.then(function (res) {
                    console.log("successfully created", JSON.stringify(res));
                    return res;
                },
                    function (error) {
                        console.error("error in creating the remark", JSON.stringify(error));
                    });
            }

            service.delete = function (landmarkRemarkId) {
                return EndPoints.deleteLandmarkRemark(landmarkRemarkId).$promise.then(function (res) {
                    console.log("successfully deleted", JSON.stringify(res));
                    return res;
                },
                    function (error) {
                        console.error("error in deleting the remark", JSON.stringify(error));
                    });
            }

            service.userRemarks = function (page) {
                return EndPoints.getUserRemark(page).$promise.then(function (res) {
                        console.log("successfully retrieving user remarks", JSON.stringify(res));
                        return res;
                },
                    function (error) {
                        console.error("error in getting user remarks", JSON.stringify(error));
                    });
            }

        });