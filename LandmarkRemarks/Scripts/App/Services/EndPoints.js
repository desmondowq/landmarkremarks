﻿angular.module('LandmarkRemarkApp.services')
	.service('EndPoints',
		function ($resource) {
		    var endPoint = this;
		    var URL = '/';
		    var accountTokenPath = 'Token';
		    var accountRegisterPath = 'api/Account/Register';
		    var accountLogoutPath = 'api/Account/logout';
		    var accountIsAuthorizePath = 'api/Account/IsAuthorize';
		    var locationRemarkPath = 'api/LandmarkRemark/:landmarkRemarkId';
		    var UserRemarkPath = 'api/LandmarkRemark/user';


		    var locationRemark = $resource(URL.concat(locationRemarkPath), {}, {
		        delete: {
		            method: 'DELETE'
		        },
		        post: {
		            method: 'POST'
		        },
		        query: {
		            method: 'GET',
		            isArray: true
		        }
		    });

		    var userRemarks = $resource(URL.concat(UserRemarkPath), {}, {
		        get: {
		            method: 'GET',
		            isArray: true
		        }
		    });

		    var token = $resource(URL.concat(accountTokenPath), {}, {
		        login: {
		            method: 'POST',
		            headers: {
		                'Content-Type': 'application/x-www-form-urlencoded'
		            },
		            then: function (resolve) {
		                this.bypassErrorsInterceptor = true;
		                this.then = null;
		                resolve(this);
		            }
		        },
		        refresh: {
		            method: 'POST',
		            headers: {
		                'Content-Type': 'application/x-www-form-urlencoded'
		            },
		            then: function (resolve) {
		                this.bypassErrorsInterceptor = true;
		                this.then = null;
		                resolve(this);
		            }
		        }
		    });

		    var register = $resource(URL.concat(accountRegisterPath),
				{},
				{
				    post: {
				        method: 'POST'
				    },
				    then: function (resolve) {
				        this.bypassErrorsInterceptor = true;
				        this.then = null;
				        resolve(this);
				    }
				});

		    var logout = $resource(URL.concat(accountLogoutPath), {}, {
		        get: {
		            method: 'GET'
		        }
		    });

		    var isAuthorize = $resource(URL.concat(accountIsAuthorizePath), {}, {
		        get: {
		            method: 'GET'
		        }
		    });

		    endPoint.getLocationRemarks = function (search) {
		        console.log(search,"search");
		        return locationRemark.query({
		            search: search || "",
		        });
		    };

		    endPoint.getUserRemark = function (page) {
		        return userRemarks.get();
		    };

		    endPoint.postLandmarkRemark = function (lat, lng, remark) {
		        return locationRemark.post({
		            remark: remark,
		            latitude: lat,
		            longitude: lng
		        });
		    };

		    endPoint.deleteLandmarkRemark = function (landmarkRemarkId) {
		        return locationRemark.delete({ landmarkRemarkId: landmarkRemarkId });
		    };


		    endPoint.accountLogin = function (username, password) {
		        var template = 'username=@username&password=@password&grant_type=password';
		        var data = template.replace('@username', username).replace('@password', password);
		        return token.login(data);
		    };
		    endPoint.accountRegister = function (registerData) {
		        return register.post(registerData);
		    };
		    endPoint.accountLogout = function () {
		        return logout.get();
		    };
		    endPoint.accountIsAuthorize = function () {
		        return isAuthorize.get();
		    };
		});