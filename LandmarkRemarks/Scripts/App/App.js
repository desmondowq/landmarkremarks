﻿
// Initialize modules
angular.module('LandmarkRemarkApp.constants', [])
    .constant('STORAGE_KEY',
        {
            'accessToken': 'accessToken',
            'username': 'username'
        });


//create a Module
angular.module('LandmarkRemarkApp.configs', []);
angular.module('LandmarkRemarkApp.services', []);
angular.module('LandmarkRemarkApp.controllers', []);
angular.module('LandmarkRemarkApp.directives', []);


angular.module('LandmarkRemarkApp',
    [
        'ui.router',
        'ngResource',
        'ngSanitize',
        'LandmarkRemarkApp.constants',
        'LandmarkRemarkApp.configs',
        'LandmarkRemarkApp.services',
        'LandmarkRemarkApp.controllers',
        'LandmarkRemarkApp.directives'
    ])
.run(function ($state, $localStorage, STORAGE_KEY, $http, $rootScope) {
    console.log("load app");

    var accessToken = $localStorage.get(STORAGE_KEY.accessToken);
    if (accessToken) {
        var username = $localStorage.get(STORAGE_KEY.username);
        $rootScope.username = username ? username : '';

        $http.defaults.headers.common.Authorization = 'bearer ' + accessToken;
    }
});

