﻿angular.module('LandmarkRemarkApp.configs')
    .config(function ($stateProvider,
        $urlRouterProvider) {


        $stateProvider
            // setup an abstract state for the sidemenu directive
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppController'
            })
            .state('app.home',
                {
                    url: '/home',
                    views: {
                        'menuContent': {
                            templateUrl: 'Templates/home.html',
                            controller: 'HomeController'
                        }
                    }
                })
            .state('app.search',
                {
                    url: '/search',
                    views: {
                        'menuContent': {
                            templateUrl: 'Templates/search.html',
                            controller: 'SearchController'
                        }
                    },
                    params: {
                        'search' : ''
                    },
                    resolve: {
                        remarks: function($stateParams, LandmarkService, $q) {
                            console.log($stateParams.search, "search params");
                            if ($stateParams.search.length) {
                                return LandmarkService.query($stateParams.search).then(function (res) {
                                    console.log(JSON.stringify(res), 'remarks in StateConfig resolve');
                                    return res;
                                }, function (error) {
                                    console.error(JSON.stringify(error), 'error');
                                    return $q.reject(error);
                                });
                            } else {
                                return [];
                            }
                        }
                    }
                })
            .state('app.user-remarks',
                {
                    url: '/user-remarks',
                    views: {
                        'menuContent': {
                            templateUrl: 'Templates/search.html',
                            controller: 'SearchController'
                        }
                    },
                    resolve: {
                        remarks: function (LandmarkService, $q) {
                            return LandmarkService.userRemarks().then(function (res) {
                                console.log(JSON.stringify(res), 'remarks in StateConfig resolve');
                                return res;
                            }, function(error) {
                                console.error(JSON.stringify(error), 'error');
                                return $q.reject(error);
                            });
                        }
                    }
                })
            .state('app.register-login',
                {
                    url: '/login-register',
                    views: {
                        'menuContent': {
                            templateUrl: 'Templates/LoginRegister.html',
                            controller: 'LoginRegisterController'
                        }
                    },
                    resolve: {
                        Auth: function (AuthService, STORAGE_KEY, $localStorage, $http, $rootScope, $state) {

                            var accessToken = $localStorage.get(STORAGE_KEY.accessToken);
                            if (accessToken) {
                                var username = $localStorage.get(STORAGE_KEY.username);
                                $rootScope.username = username ? username : '';
                                $http.defaults.headers.common.Authorization = 'bearer ' + accessToken;
                            }

                            AuthService.isAuthorize().then(function (res) {
                                //add user login
                                console.log("user is already log in");
                                $state.go('home');
                            }, function (error) {
                                console.log("not log in");
                            });

                        }
                    }
                });
        

        var defaultRoute = '/app/home';
        $urlRouterProvider.otherwise(defaultRoute);
    });