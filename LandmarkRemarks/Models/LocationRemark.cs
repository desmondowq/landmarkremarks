﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace LandmarkRemarks.Models
{
    public class LandmarkRemark
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public DbGeography Geo { get; set; }
        [Index]
        [MaxLength(256)]
        public string Remarks { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}