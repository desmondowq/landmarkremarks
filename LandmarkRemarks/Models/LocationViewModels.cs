﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LandmarkRemarks.Models
{
    public class LandmarkBindingModel
    {
        [Required]
        [MaxLength(256)]
        public string Remark { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }
    }

    //public class SearchLandmarkRemark
    //{
    //    public List<LandmarkRemarkViewModel> Remarks { get; set; }
    //    public int? NextPage { get; set; }
    //    public bool ReadMore { get; set; }
    //}

    public class LandmarkRemarkViewModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Remark { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Self { get; set; }
    }
}